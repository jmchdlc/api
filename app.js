const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

const init = async () => {
    
    app.use(bodyParser.json());

    const { Client } = require('pg');

    const client = new Client({
	host: 'localhost',
	port: 5432,
	user: 'postgres',
	password: 'docker',
	database: 'bank'
    });

    client.connect();

    app.get('/', (req, res) => res.send('Hello to my bank'));

    app.post('/branch/:branchId/client/:clientId/account', async (req, res) => {
	const branchId = req.params.branchId;
	const clientId = req.params.clientId;
	const body = req.body;
	
	const query = 'INSERT INTO account("nip", "branch_id", "client_id", "account_type") values($1, $2, $3, $4)';

	try {
	    const result = await client.query(query, [body.nip, branchId, clientId, body.account_type]);
	    res.send("OK");
	} catch (e) {
	    console.log(e);
	    res.status(500).send("ERROR");
	}
    });

    
    app.get('/account/checkoutBalance', async (req, res, next) => {
	const nip = req.query.nip;
        const account_type = req.query.account_type;

	console.log(req.query);

	const query = 'SELECT * FROM account WHERE nip = $1 AND account_type = $2';

        try {
            const result = await client.query(query, [nip, account_type]);

	    if (result.rows[0].balance > 0) {
		req.balance = result.rows[0].balance;
		next();
	    } else {
		res.status(400).send("Saldo insuficiente");
	    }		
        } catch (e) {
            console.log(e);
            res.status(500).send("ERROR");
        }
    });
    
    app.get('/account/checkoutBalance', async (req, res) => {
	    res.send(req.balance);
    });

    app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

}

init();
